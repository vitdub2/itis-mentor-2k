package repositories;

import java.util.List;

public interface CRUDRepository <T> {
    T save (T entity);
    T findById (int id);
    List<T> findAll();
}
