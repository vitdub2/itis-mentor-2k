package repositories.user;

import models.User;
import repositories.CRUDRepository;

public interface UserRepository extends CRUDRepository<User> {
}
