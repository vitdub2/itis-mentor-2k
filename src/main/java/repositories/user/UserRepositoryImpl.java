package repositories.user;

import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository{
    private final Connection connection;

    public UserRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public User save(User entity) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into users (email, password) values (?, ?)");
            statement.setString(1, entity.getEmail());
            statement.setString(2, entity.getPassword());

            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;
    }

    @Override
    public User findById(int id) {
        User user = null;
        try {
            PreparedStatement statement = connection.prepareStatement("select * from users where id=?");
            statement.setInt(1, id);

            ResultSet set = statement.executeQuery();
            while (set.next()) {
                String email = set.getString("email");
                String password = set.getString("password");

                user = User.builder()
                        .id(id)
                        .email(email)
                        .password(password)
                        .build();

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery("select * from users");
            while (set.next()) {
                int id = set.getInt("id");
                String email = set.getString("email");
                String password = set.getString("password");

                User user = User.builder()
                        .id(id)
                        .email(email)
                        .password(password)
                        .build();

                users.add(user);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return users;
    }
}
