package servlets;

import models.User;
import repositories.user.UserRepository;
import repositories.user.UserRepositoryImpl;
import utils.ConnectionManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/profile/*")
public class ProfileServlet extends HttpServlet {
    private UserRepository userRepository;

    @Override
    public void init() {
        try {
            this.userRepository = new UserRepositoryImpl(ConnectionManager.getConnection());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getPathInfo();
        int id = Integer.parseInt(path.split("/")[1]);
        User user = userRepository.findById(id);

        req.setAttribute("user", user);
        req.getRequestDispatcher("/jsp/profile.jsp").forward(req, resp);
    }
}
