package servlets;

import models.User;
import repositories.user.UserRepository;
import repositories.user.UserRepositoryImpl;
import utils.ConnectionManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {
    private UserRepository userRepository;

    @Override
    public void init() {
        try {
            this.userRepository = new UserRepositoryImpl(ConnectionManager.getConnection());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  {
        List<User> users = userRepository.findAll();
        System.out.println(users);
        try {
            req.setAttribute("users", users);
            req.getRequestDispatcher("/jsp/main.jsp").forward(req, resp);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        User user = User.builder()
                .password(password)
                .email(email)
                .build();

        System.out.println(user);

        userRepository.save(user);

        resp.sendRedirect("/hello");
    }
}
