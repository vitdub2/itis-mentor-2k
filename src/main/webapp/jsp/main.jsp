<%--
  Created by IntelliJ IDEA.
  User: vitdub
  Date: 11.09.2021
  Time: 21:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main page</title>
</head>
<body>
    <h1>
        Hello world!
    </h1>

    <form action="/hello" method="post">
        <input type="email" name="email" placeholder="E-mail">
        <input type="password" name="password" placeholder="Password">
        <button type="submit">
            Send
        </button>
    </form>
    <ul>
        <c:forEach items="${users}" var="user">
            <li>
                <a href="/profile/${user.id}">
                    ${user.email}
                </a>
            </li>
        </c:forEach>
    </ul>
</body>
</html>
